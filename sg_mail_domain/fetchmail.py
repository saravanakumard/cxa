from openerp import models, fields, api, _
from openerp.osv import fields as ofields
from openerp.osv import osv
from openerp import netsvc
from openerp.exceptions import Warning
from datetime import datetime as time
import openerp.addons.decimal_precision as dp    
import logging
import poplib
import time
from imaplib import IMAP4
from imaplib import IMAP4_SSL
from poplib import POP3
from poplib import POP3_SSL
from email import _name
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

import zipfile
import base64
from openerp import addons
from openerp import tools, api
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)
MAX_POP_MESSAGES = 50
MAIL_TIMEOUT = 60





# class fetchmail_server(osv.osv):
#     """Incoming POP/IMAP mail server account"""
#     _inherit = 'fetchmail.server'
#     def fetch_mail(self, cr, uid, ids, context=None):
#         """WARNING: meant for cron usage only - will commit() after each email!"""
#         context = dict(context or {})
#         context['fetchmail_cron_running'] = True
#         mail_thread = self.pool.get('mail.thread')
#         action_pool = self.pool.get('ir.actions.server')
#         for server in self.browse(cr, uid, ids, context=context):
#             _logger.info('start checking for new emails on %s server %s', server.type, server.name)
#             context.update({'fetchmail_server_id': server.id, 'server_type': server.type})
#             count, failed = 0, 0
#             imap_server = False
#             pop_server = False
#             if server.type == 'imap':
#                 try:
#                     imap_server = server.connect()
#                     imap_server.select()
#                     result, data = imap_server.search(None, '(UNSEEN)')
#                     for num in data[0].split():
#                         res_id = None
#                         result, data = imap_server.fetch(num, '(RFC822)')
#                         imap_server.store(num, '-FLAGS', '\\Seen')
#                         try:
#                             print 'context fetch mail  ', data
#                             # Gia tri email tim duoc trong luc fetch mail - NHT
#                             #email = msg.split('Return-Path: ')[1].split('>')[0].split('<')[1] 
#                             res_id = mail_thread.message_process(cr, uid, server.object_id.model,
#                                                                  data[0][1],
#                                                                  save_original=server.original,
#                                                                  strip_attachments=(not server.attach),
#                                                                  context=context)
#                         except Exception:
#                             _logger.exception('Failed to process mail from %s server %s.', server.type, server.name)
#                             failed += 1
#                         if res_id and server.action_id:
#                             action_pool.run(cr, uid, [server.action_id.id], {'active_id': res_id, 'active_ids': [res_id], 'active_model': context.get("thread_model", server.object_id.model)})
#                         imap_server.store(num, '+FLAGS', '\\Seen')
#                         cr.commit()
#                         count += 1
#                     _logger.info("Fetched %d email(s) on %s server %s; %d succeeded, %d failed.", count, server.type, server.name, (count - failed), failed)
#                 except Exception:
#                     _logger.exception("General failure when trying to fetch mail from %s server %s.", server.type, server.name)
#                 finally:
#                     if imap_server:
#                         imap_server.close()
#                         imap_server.logout()
#             elif server.type == 'pop':
#                 try:
#                     while True:
#                         pop_server = server.connect()
#                         (numMsgs, totalSize) = pop_server.stat()
#                         pop_server.list()
#                         for num in range(1, min(MAX_POP_MESSAGES, numMsgs) + 1):
#                             (header, msges, octets) = pop_server.retr(num)
#                             msg = '\n'.join(msges)
#                             res_id = None
#                             try:
#                                 # Gia tri email tim duoc trong luc fetch mail - NHT
#                                 old_email = msg.split('Return-Path: ')[1].split('>')[0].split('<')[1]
#                                 email = old_email.split('@')[1]
#                                 user_domain_obj = self.pool.get('res.users.line')
#                                 st_domain_obj  = self.pool.get('crm.case.section.line')
#                                 uids = user_domain_obj.search(cr, uid, [('name', '=', email)])
#                                 uids = uids and [x.user_id.partner_id.email for x in user_domain_obj.browse(cr, uid, uids) if x.user_id.partner_id.email] or []
#                                 
#                                 uids1 = st_domain_obj.search(cr, uid, [('name', '=', email)])
#                                 if uids1:
#                                     for y in st_domain_obj.browse(cr, uid, uids1):
#                                         uids += [z.partner_id.email for z in y.sale_team_id.member_ids if z.partner_id.email]
#                                 uids = list(set(uids))
#                                 if old_email in uids:
#                                     uids.remove(old_email)
#                                 tmp = ''
#                                 for u in uids:
#                                     tmp += u + ',' 
#                                 context.update({'email_to': tmp})
#                                 print 'gia tri tmp  ==   ', context
#                                 if tmp:
#                                     msg = msg.replace('Delivered-To:', 'Delivered-To: %s'%tmp)
# #                                 print 'Dang check ==  ', msg
#                                 res_id = mail_thread.message_process(cr, uid, server.object_id.model,
#                                                                      msg,custom_values={'email':tmp},
#                                                                      save_original=server.original,
#                                                                      strip_attachments=(not server.attach),
#                                                                      context=context)
#                                 pop_server.dele(num)
#                             except Exception:
#                                 _logger.exception('Failed to process mail from %s server %s.', server.type, server.name)
#                                 failed += 1
#                             if res_id and server.action_id:
#                                 action_pool.run(cr, uid, [server.action_id.id], {'active_id': res_id, 'active_ids': [res_id], 'active_model': context.get("thread_model", server.object_id.model)})
#                             cr.commit()
#                         if numMsgs < MAX_POP_MESSAGES:
#                             break
#                         pop_server.quit()
#                         _logger.info("Fetched %d email(s) on %s server %s; %d succeeded, %d failed.", numMsgs, server.type, server.name, (numMsgs - failed), failed)
#                 except Exception:
#                     _logger.exception("General failure when trying to fetch mail from %s server %s.", server.type, server.name)
#                 finally:
#                     if pop_server:
#                         pop_server.quit()
#             server.write({'date': time.strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)})
#         return True
    
    
class crm_case_section_line(models.Model):
    _name = 'crm.case.section.line'
    
    name = fields.Char('Domain')
    subject_check = fields.Char('Subject')
    team_domain = fields.Many2one('domain.email', 'Domain Configuration')
    sale_team_id = fields.Many2one('crm.case.section', string='Sales Team')
    
class res_users_line(models.Model):
    _name = 'res.users.line'
        
    name = fields.Char('Domain')
    subject_check_user = fields.Char('Subject')
    user_domain = fields.Many2one('domain.email', 'Domain Configuration')
    user_id = fields.Many2many('res.users', string='Sale Man')

class domain_email(models.Model):
    _name = 'domain.email'
        
    name = fields.Char('Domain')
    domain_email_for_all = fields.Many2one('res.users', string='Domain Email')
    saleteam_domain_ids = fields.One2many('crm.case.section.line', 'team_domain' ,'Sales Team Domain' )
    saleman_domain = fields.One2many('res.users.line', 'user_domain', 'Sale Man Domain')
        
class crm_helpdesk(models.Model):
    _inherit = 'crm.helpdesk'

    respond_id = fields.Many2one('res.users', string='Responsible')
    
    def update_helpdesk(self, cr, uid, context=None):
        ids = self.pool.get('crm.helpdesk').search(cr, uid, [])
        for x in ids:
            user_id = self.pool.get('crm.helpdesk').browse(cr, uid, x).user_id
            user_id = user_id and user_id.id or 0
            if not self.pool.get('crm.helpdesk').browse(cr, uid, x).respond_id:
                self.pool.get('crm.helpdesk').write(cr, uid, [x], {'respond_id': user_id})
        return 1
    
class mail_message(models.Model):
    _inherit = 'mail.message'
    
    def create(self, cr, uid, values, context=None):
        context = dict(context or {})
        message_id = super(mail_message, self).create(cr, uid, values, context)
        message = self.browse(cr, uid, message_id)
        uids = fallback_uids = []
        if message.type == 'email' or message.mail_type == 'in':
            message_vals = {
                'name': message.record_name or message.subject,
                'description': message.body,
                }
            partner_ids = 0
            if message.email_from:
                email = message.email_from
                if '<' in email and '>' in email:
                    email = email[email.index('<')+1:email.index('>')]
                partner_ids = self.pool.get('res.partner').search(cr, uid, [('email', '=', email)])
                if partner_ids:
                    message_vals.update({'partner_id': partner_ids[0]})


            email = email.split('@')[1]
            # Code Start For Route email to selected user (Customised by Parikshit Vaghasiya)
            user_domain_obj = self.pool.get('res.users.line')
            st_domain_obj  = self.pool.get('crm.case.section.line')
            subje_uids = user_domain_obj.search(cr, uid, [('name', '=', email)])
            uids_dim = []
            for subje_id in subje_uids:
                # ('subject_check_user','in',str(message.subject).split(' '))
                subjec_obj = user_domain_obj.browse(cr, uid, subje_id).subject_check_user.split(',')
                for sub in subjec_obj:
                    for i in str(message.subject).split(' '):
                        if sub.strip() == i.strip():
                            uids_dim.append(subje_id)
            
            for uids_od in uids_dim:
                for user_dom in user_domain_obj.browse(cr, uid, uids_od).user_id:
                    uids.append(user_dom.id)

            uids1 = []
            subje_uids1 = st_domain_obj.search(cr, uid, [('name', '=', email)])
            for subj_id in subje_uids1:
                # ('subject_check','in',str(message.subject).split(' '))
                subj_obj = st_domain_obj.browse(cr, uid, subj_id).subject_check.split(',')
                for subi in subj_obj:
                    for j in str(message.subject).split(' '):
                        if subi.strip() == j.strip():
                            uids1.append(subj_id)

            if uids1:
                for y in st_domain_obj.browse(cr, uid, uids1):
                    uids += [z.id for z in y.sale_team_id.member_ids if y.sale_team_id.member_ids]
            uids = list(set(uids))
            if uids:
                for p in uids:
                    pt_id = self.pool.get('res.users').browse(cr, uid, p).partner_id.id
                    if pt_id:
                        message.partner_ids = [(4,pt_id)]
                        message.notified_partner_ids = [(4,pt_id)]
        
            if not uids:
                dom_obj_ek = self.pool.get('domain.email').search(cr, uid, [])
                if dom_obj_ek:
                    dom_obj = dom_obj_ek[0]
                    dom = self.pool.get('domain.email').browse(cr, uid, dom_obj)
                    if dom and dom.domain_email_for_all:
                        emai = dom.domain_email_for_all.partner_id.id
                        fallback_uids.append(dom.domain_email_for_all)
                        message.partner_ids = [(4,emai)]
                        message.notified_partner_ids = [(4,emai)]
            # Code End For Route email to selected user (Customised by Parikshit Vaghasiya)

            # user_domain_obj = self.pool.get('res.users.line')
            # st_domain_obj  = self.pool.get('crm.case.section.line')
            # uids_i = user_domain_obj.search(cr, uid, [('name', '=', email)])
            # uids = uids and [x.id for x in user_domain_obj.browse(cr, uid, uids).user_id if x.id] or []
            # uids = []
            # for uids_iam in uids_i:
            #     for uid_u in user_domain_obj.browse(cr, uid, uids_iam).user_id:
            #         uids.append(uid_u.id)
            # uids1 = st_domain_obj.search(cr, uid, [('name', '=', email)])
            # if uids1:
            #     for y in st_domain_obj.browse(cr, uid, uids1):
            #         uids += [z.id for z in y.sale_team_id.member_ids if y.sale_team_id.member_ids]
            # uids = list(set(uids))
            helpdesk_obj = self.pool.get('crm.helpdesk')
            if uids:
                for k in uids:
                    helpdesk_obj.create(cr, uid, {'name': message.record_name or message.subject,
                    'description': message.body,
                    'email_from':message.email_from,
                    'partner_id': partner_ids and partner_ids[0] or 0,
                    'respond_id': k})
            elif fallback_uids:
                print "SSSSSSSSS",fallback_uids
                for j in fallback_uids:
                    helpdesk_obj.create(cr, uid, {'name': message.record_name or message.subject,
                    'description': message.body,
                    'email_from':message.email_from,
                    'partner_id': partner_ids and partner_ids[0] or 0,
                    'respond_id': j.id,})
        return message_id
    
    