# -*- encoding: utf-8 -*-

{
    "name": "Domain incoming Mails",
    "version": "1.0",
    "depends": ['base', 'sg_mail', 'sales_team', 'sg_helpdesk'],
    "author": "HashMicro /Minh Tran / Parikshit Vaghasiya",
	"website": "www.hashmicro.com",
    "category": "Customize",
    "sequence": 3,
    "summary": "Domain incoming Mails",
    "description": """
		New menu Sent Mails in Messenging
		1.	Incoming email 
		-	Filter based on domain. Ex: @gmail, @hashmicro…
		-	Each domain assigned to a pre-defined User 
		-	Auto assign this user to helpdesk ticket which auto generate when incoming email comes (Responsible field in Sales / After-Sale Services / Helpdesk and Support)
		2.	Messaging / Mails / Inbox menu
		-	Store incoming emails (not messages) of this user
		-	Can be searched by Sender, Subject, and a HTML content
		-	Can be composed new email in this menu
		3.	 Messaging / Mails / Sent Mails menu
		-	Store outgoing emails (not messages) of this user
		-	Can be searched by Receipient, Subject, and a HTML content

    """,
    "init_xml": [],
    'update_xml': [
        'views/mail_mail_view.xml',

    ],
    'js': [

    ],
    'css': [

    ],
    'qweb': ['static/src/xml/image_multi.xml'],
    'demo_xml': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}