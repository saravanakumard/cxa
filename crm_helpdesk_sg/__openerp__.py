# -*- coding: utf-8 -*-

{
    'name': 'CRM Helpdesk',
    'version': '1.4',
    'summary': 'CRM Helpdesk',
    'description': """
        Customisations in CRM Helpdesk
    """,
    'author': 'HashMicro / Janeesh',
    'website': 'www.hashmicro.com',
    'category': 'Customer Relationship Management',
    'sequence': 0,
    'images': [],
    'depends': ['base', 'crm_helpdesk', 'crm'],
    'demo': [],
    'data': [
        'views/crm_helpdesk_view.xml',
        'views/res_users_view.xml',
        'views/res_partner_view.xml',
        'sequence.xml'
        ],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: