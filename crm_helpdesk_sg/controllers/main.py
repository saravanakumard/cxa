# -*- coding: utf-8 -*-
import werkzeug

from openerp import http
from openerp.http import request
from openerp import SUPERUSER_ID

class website_nric(http.Controller):
    
    @http.route('/nric', type="http", auth="user")
    def render_helpdesk(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        redirect_url, partner_ids = '/', []
        partner_obj = pool.get('res.partner')
        helpdesk_obj = pool.get('crm.helpdesk')
        categ_obj = pool.get('crm.case.categ')
        partner_ids = []
        if not 'type' in post:
            return werkzeug.utils.redirect(redirect_url)
        categ_ids = categ_obj.search(cr, uid, [('type', '=', int(post['type']))])
        categ_name, categ_id = '', False
        if categ_ids:
            categ = categ_obj.browse(cr, uid, categ_ids[0])
            categ_name, categ_id = categ.name, categ.id
        if not categ_id:
            return werkzeug.utils.redirect(redirect_url)
        if 'uid' in post:
            partner_ids = partner_obj.search(cr, uid, [('customer_id', '=', post['uid'])], context=context)
        if not partner_ids and 'cid' in post:
            partner_ids = partner_obj.search(cr, uid, [('phone', '=', post['cid'])], context=context)
            
        redirect_url = '/web#id=%(helpdesk_id)d&view_type=form&model=crm.helpdesk&menu_id=%(menu_id)d&action=%(action_id)d'
        params = {}
        partner_id = partner_ids and partner_ids[0] or False
        helpdesk_id = helpdesk_obj.create(cr, uid, {
            'name': categ_name + ' -',
            'partner_id': partner_id,
            'categ_id': categ_id
            })
        params['helpdesk_id'] = helpdesk_id
        
        pool.get('crm.phonecall').create(cr, uid, {
            'helpdesk_id': helpdesk_id,
            'partner_id': partner_id,
            'name': categ_name
            })
        
        menu_id = pool.get('ir.model.data').get_object_reference(cr, SUPERUSER_ID, 'crm_helpdesk', 'menu_help_support_main')
        if menu_id:
            params['menu_id'] = menu_id[1]
            menu_obj = pool.get('ir.ui.menu')
            menu = menu_obj.browse(cr, uid, [menu_id[1]], context=context)
            params['action_id'] = menu.action.id
            redirect_url = redirect_url%params
        return werkzeug.utils.redirect(redirect_url)

# vim:expandtab:tabstop=4:softtabstop=4:shiftwidth=4:
