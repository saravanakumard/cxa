# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class crm_helpdesk(models.Model):
    _inherit = 'crm.helpdesk'

    @api.onchange('nric')
    def on_change_nric(self):
        if self.nric:
            partner = self.env['res.partner'].search([('nric', '=', self.nric)])
            if partner and partner.id:
                self.company_id     = partner.parent_id
                self.employee_name  = partner.name
                self.entity         = partner.is_company
                self.cate_employee  = partner.category_id
            else:
                self.company_id     = None
                self.employee_name  = None
                self.entity         = False
                self.cate_employee  = None
        else:
            self.company_id         = None
            self.employee_name      = None
            self.entity             = False
            self.cate_employee      = None

    number          = fields.Char('Number')
    categ_id        = fields.Many2one('crm.case.categ', 'Tag')
    customer_name   = fields.Char('Customer Name', related='partner_id.name', readonly=True)
    street          = fields.Char('Street', related='partner_id.street', readonly=True)
    street2         = fields.Char('Street2', related='partner_id.street2', readonly=True)
    city            = fields.Char('City', related='partner_id.city', readonly=True)
    zip             = fields.Char('Zip', related='partner_id.zip', readonly=True)
    state_id        = fields.Many2one('res.country.state', 'State', related='partner_id.state_id', readonly=True)
    country_id      = fields.Many2one('res.country', 'Country', related='partner_id.country_id', readonly=True)
    email           = fields.Char('Email', related='partner_id.email', readonly=True)
    phone           = fields.Char('Phone', related='partner_id.phone', readonly=True)
    fax             = fields.Char('Fax', related='partner_id.fax', readonly=True)
    mobile          = fields.Char('Mobile', related='partner_id.mobile', readonly=True)
    customer_id     = fields.Char('ID', related='partner_id.customer_id', readonly=True)
    website         = fields.Char('Website', related='partner_id.website', readonly=True)

    company_id      = fields.Many2one('res.partner', 'Company')
    entity          = fields.Boolean('Entity')
    employee_name   = fields.Char('Employee Name')
    cate_employee   = fields.Many2many('res.partner.category', string='Categorization Employee')
    nric            = fields.Char('NRIC')

    request_type_id = fields.Many2one('crm.request.type', 'type_id')
    complain        = fields.Boolean('Complain')
    urgent          = fields.Boolean('Urgent')
    method_type     = fields.Selection([
        ('call' , 'Call'),
        ('email' , 'Email')
    ])
    follow_action   = fields.Text('Follow Up Action')
    follow_date     = fields.Date('Follow Up Date')
    department      = fields.Many2one('crm.case.section',string='Departments', states={'draft': [('readonly', True)], 'pending': [('readonly', False)]})
    assignee        = fields.Many2one('res.users', string='Assignee', related='department.user_id',  states={'draft': [('readonly', True)], 'pending': [('readonly', False)]})

    def create(self, cr, uid, vals, context=None):
        vals.update({'number': self.pool.get('ir.sequence').get(cr, uid, 'crm.help', context=context)})
        return super(crm_helpdesk, self).create(cr, uid, vals, context=context)
    
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        user = self.pool.get('res.users').browse(cr, uid, uid)
        categ_ids = [categ.id for categ in user.categ_ids]
        args.extend(['|', ('categ_id', 'in', categ_ids), ('categ_id', '=', False)])
        res = super(crm_helpdesk, self).search(cr, uid, args=args, offset=offset, limit=limit, order=order, context=context)
        return res

class crm_case_categ(models.Model):
    _inherit = 'crm.case.categ'
    
    type = fields.Integer('Type')

class crm_request_type(models.Model):
    _name = 'crm.request.type'

    name    = fields.Char('Type Name')
    type_id = fields.Many2one('crm.helpdesk', readonly=True, string='ID')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: