# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class crm_phonecall(models.Model):
    _inherit = 'crm.phonecall'
    
    helpdesk_id = fields.Many2one('crm.helpdesk', 'Ticket')
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: