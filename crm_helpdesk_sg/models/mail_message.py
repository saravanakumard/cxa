# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class mail_message(models.Model):
    _inherit = 'mail.message'
    
    def create(self, cr, uid, values, context=None):
        context = dict(context or {})
        message_id = super(mail_message, self).create(cr, uid, values, context)
        message = self.browse(cr, uid, message_id)
        if message.type == 'email' or message.mail_type == 'in':
            message_vals = {
                'name': message.record_name or message.subject,
                'description': message.body
                }
            if message.email_from:
                email = message.email_from
                if '<' in email and '>' in email:
                    email = email[email.index('<')+1:email.index('>')]
                print ">>>>>>>>>>>>>DDDDDDDDDVVVVVVVVVVVVVVVVVVVVVVVVVVVVV",email
                partner_ids = self.pool.get('res.partner').search(cr, uid, [('email', '=', email)])
                if partner_ids:
                    message_vals.update({'partner_id': partner_ids[0]})

            # self.pool.get('crm.helpdesk').create(cr, uid, message_vals)
        return message_id
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: