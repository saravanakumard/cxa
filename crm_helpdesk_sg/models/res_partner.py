# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class res_partner(models.Model):
    _inherit = 'res.partner'

    customer_id = fields.Char('ID')
    helpdesk_id = fields.Many2one('crm.helpdesk', 'Helpdesk')

    nric        = fields.Char('NRIC')

    _sql_constraints = [
        ('nric', 'unique(nric)', 'NRIC already exists!'),
    ]
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: