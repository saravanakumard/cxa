# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class res_users(models.Model):
    _inherit = 'res.users'

    categ_ids   = fields.Many2many('crm.case.categ', 'users_category_rel', 'user_id', 'category_id', 'Tags')

    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: