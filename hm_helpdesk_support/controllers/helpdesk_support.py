# -*- coding: utf-8 -*-
import werkzeug

from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.http import request

class HmHelpdeskSupport(http.Controller):

    @http.route('/hm_helpdesk_support/', auth='public')
    def hm_index(self, cl, equiry, **kw):
        if cl:
            # create crm.helpdesk
            helpdesk_obj = request.registry.get('crm.helpdesk')
            enquiry_obj = request.registry.get('crm.helpdesk.enquiry')

            helpdesk_data = {
                'name': 'CXA - %s' %(cl,),
                'enquiry_id': enquiry_obj.get_enquiry_by_code(request.cr, request.uid, equiry)
            }
            ticket_id = helpdesk_obj.create(request.cr, request.uid, helpdesk_data)

            if ticket_id:
                ticket_url = '/web#id=%s&view_type=form&model=crm.helpdesk' % (ticket_id,)
                return werkzeug.utils.redirect(ticket_url)

        return "Invalid Request"