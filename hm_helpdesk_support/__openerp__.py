# -*- coding: utf-8 -*-
{
    'name': "hm_helpdesk_support",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "HashMicro / Sang",
    'website': "http://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'HashMicro',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'crm',
        'crm_helpdesk',
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/templates.xml',
        'views/hm_helpdesk_support_view.xml',
        'views/hm_helpdesk_support_menus.xml',
        'views/hm_enquiry_view.xml',
        'views/hm_type_view.xml',
        'views/hm_remark_view.xml',
        'data/sample.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}