# -*- coding: utf-8 -*-

import openerp
from openerp.addons.crm import crm
from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
from openerp.tools import html2plaintext

class HmHelpdeskReport(osv.osv):
    _inherit   = 'crm.helpdesk'

    _columns   = {
        'enquiry_id': fields.many2one('crm.helpdesk.enquiry', 'Enquiry Option'),
        'type_id': fields.many2one('crm.helpdesk.type', 'Query Type', domain="[('enquiry_id', '=', enquiry_id)]"),
        'remark_id': fields.many2one('crm.helpdesk.remark', 'Query Remark', domain="[('type_id', '=', type_id)]"),
        'state': fields.selection(
            [('draft', 'Open'),
             ('pending', 'Pending'),
             ('resolved', 'Resolved'),
             ('closed', 'Closed'),
             ('cancel', 'Cancelled')], 'Status', readonly=True, track_visibility='onchange',
                              help='Open status is automatic when there is no responsible person.\
\nIf there is a responsible person, it will auto go to pending\
\nIt is resolved if the responsible person click on "resolved" to change the status.\
\nBut responsible person has the option to move back to pending and vice versa.\
\n\
\n\
\nClosed status is automatic after 1.5 months in resolved status.\
\nBut responsible person has the option to click on closed to change the status\
\nBut responsible person has the option to move back to pending and vice versa.'),
    }

class HmHelpdeskEnquiry(osv.osv):
    _name      = 'crm.helpdesk.enquiry'

    _columns   = {
        'name': fields.char('Name'),
        'code': fields.char('Code'),
    }

    def _get_enquiry_by_code(self, cr, uid, code, context=None):
        result = None

        domain = [('code', '=', code)]
        ids    = self.search(cr, uid, domain, context=context)
        if ids and len(ids) > 0:
            result = ids[0]

        return result

    def get_enquiry_by_code(self, cr, uid, code, context=None):
        id = self._get_enquiry_by_code(cr, uid, code, context=context)
        if not id:
            id = self._get_enquiry_by_code(cr, uid, 'enquiry_id', context=context)
        return id

class HmHelpdeskType(osv.osv):
    _name      = 'crm.helpdesk.type'

    _columns   = {
        'name': fields.char('Name'),
        'code': fields.char('Code'),
        'enquiry_id': fields.many2one('crm.helpdesk.enquiry', 'Enquiry'),
    }

class HmHelpdeskRemark(osv.osv):
    _name      = 'crm.helpdesk.remark'

    _columns   = {
        'name': fields.char('Name'),
        'code': fields.char('Code'),
        'type_id': fields.many2one('crm.helpdesk.type', 'Type'),
    }
