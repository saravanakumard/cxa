# -*- coding: utf-8 -*-

{
    'name': 'CRM Helpdesk Custom',
    'version': '1.4',
    'summary': 'CRM Helpdesk',
    'description': """
        Customisations in CRM Helpdesk
    """,
    'author': 'HashMicro / Janeesh',
    'website': 'www.hashmicro.com',
    'category': 'Customer Relationship Management',
    'sequence': 0,
    'images': [],
    'depends': ['crm_helpdesk', 'crm_helpdesk_sg', 'mail', 'sg_mail'],
    'demo': [],
    'data': [
        'views/crm_phonecall_view.xml',
        'views/crm_helpdesk_view.xml',
        ],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: