# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class crm_phonecall(models.Model):
    _inherit = 'crm.phonecall'
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if 'default_helpdesk_id' in context:
            helpdesk_obj = self.pool.get('crm.helpdesk')
            helpdesk = helpdesk_obj.browse(cr, uid, context['default_helpdesk_id'])
            helpdesk_ids = [helpdesk.id]
            parent = helpdesk.parent_id
            while parent:
                helpdesk_ids.append(parent.id)
                parent = parent.parent_id
            helpdesk_ids2 = helpdesk_obj.search(cr, uid, [('parent_id', '=', helpdesk.id)])
            while helpdesk_ids2:
                for helpdesk_id in helpdesk_ids2:
                    helpdesk_ids.append(helpdesk_id)
                    helpdesk_ids2 = helpdesk_obj.search(cr, uid, [('parent_id', '=', helpdesk_id)])
            args.extend([('helpdesk_id', 'in', helpdesk_ids)])
        return super(crm_phonecall, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context)
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: