# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class crm_helpdesk(models.Model):
    _inherit = 'crm.helpdesk'
    _rec_name = 'number'
    
    @api.one
    def _get_calls(self):
        phonecall_count = 0
        helpdesk_ids = [self.id]
        parent = self.parent_id
        while parent:
            helpdesk_ids.append(parent.id)
            parent = parent.parent_id
        helpdesk_ids2 = self.search([('parent_id', '=', self.id)])
        while helpdesk_ids2:
            for helpdesk in helpdesk_ids2:
                helpdesk_ids.append(helpdesk.id)
                helpdesk_ids2 = self.search([('parent_id', '=', helpdesk.id)])
        phonecall_ids = self.env['crm.phonecall'].search([('helpdesk_id', 'in', helpdesk_ids)])
        if phonecall_ids:
            phonecall_count = len(phonecall_ids)
        self.phonecall_count = phonecall_count
    
    @api.one
    def _tickets(self):
        ticket_open_ids, ticket_closed_ids = [], []
        if self.partner_id:
            open_state = ('draft', 'open', 'pending')
            ticket_open = self.search([('id', '!=', self.id), ('partner_id', '=', self.partner_id.id), ('state', 'in', open_state)])
            ticket_open_ids = [ticket.id for ticket in ticket_open]
            ticket_closed = self.search([('id', '!=', self.id), ('partner_id', '=', self.partner_id.id), ('state', '=', 'done')])
            ticket_closed_ids = [ticket.id for ticket in ticket_closed]
        self.ticket_open_ids = ticket_open_ids
        self.ticket_closed_ids = ticket_closed_ids
    
    @api.one
    def _history(self):
        history_ids = []
        parent = self.parent_id
        while parent:
            history_ids.append(parent.id)
            parent = parent.parent_id
        self.history_ids = history_ids
        
    phonecall_count = fields.Integer('Phonecalls', compute=_get_calls)
    type = fields.Selection([('in', 'Inbound'), ('out', 'Outbound')], 'Type', default='in')
    parent_id = fields.Many2one('crm.helpdesk', 'Reference')
    ticket_open_ids = fields.Many2many('crm.helpdesk', 'ticket_id1', 'ticket_id2', 'ticket_open_rel', 'Open Tickets', compute=_tickets)
    ticket_closed_ids = fields.Many2many('crm.helpdesk', 'ticket_id1', 'ticket_id2', 'ticket_closed_rel', 'Closed Tickets',
        compute=_tickets)
    history_ids = fields.Many2many('crm.helpdesk', 'ticket_id1', 'ticket_id2', 'ticket_history_rel', 'Conversation History', 
        compute=_history)
    description = fields.Html('Description')
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: