# -*- coding: utf-8 -*-

{
    'name': 'Mail Custom',
    'version': '1.0',
    'summary': 'Mail Custom',
    'description': """
        Customizations in Mail
    """,
    'author': 'HashMicro / Janeesh',
    'website': 'www.hashmicro.com',
    'category': 'Social Network',
    'sequence': 0,
    'images': [],
    'depends': [
        'mail',
        'sale',
        'crm',
        'fetchmail',
    ],
    'demo': [],
    'data': [
        'views/mail_compose_message_view.xml',
        'views/mail_message_view.xml',
        'views/partner_view.xml',
        'views/sg_mail_report_view.xml',
        'views/fetchmail_server.xml',
        'security/mail_security.xml',
    ],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: