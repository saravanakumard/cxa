# -*- coding: utf-8 -*-

from openerp.osv import osv, orm, fields

class mail_notification(osv.Model):
    _inherit = 'mail.notification'

    _columns = {
        'read_date': fields.datetime('Read Date'),
    }

    def write(self, cr, uid, ids, values, context=None):
        if values.get('is_read', False):
            values['read_date'] = fields.datetime.now()
        result = super(mail_notification, self).write(cr, uid, ids, values, context=context)
        return result