# -*- coding: utf-8 -*-
from openerp import models, fields, api, _

class mail_compose_message(models.Model):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self):
        context = self._context
        result = super(mail_compose_message, self).send_mail()
        if context.get('default_composition_mode', False) == 'reply':
            mail_message_id = context.get('default_parent_id', False)
            if mail_message_id:
                mail = self.env['mail.mail'].search([('mail_message_id', '=', mail_message_id)])
                if mail and mail.id:
                    result = {
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'mail.mail',
                        'target': 'current',
                        'res_id': mail.id,
                        'type': 'ir.actions.act_window'
                    }
        return result