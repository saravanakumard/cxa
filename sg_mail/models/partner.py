# -*- coding: utf-8 -*-

from openerp import models, api, _, fields

class res_partner(models.Model):
    _inherit = 'res.partner'
    
    
    @api.multi
    def action_mails(self):
        message_obj = self.env['mail.message']
        messages = message_obj.search([('has_mail', '=', True)])
        message_ids = [message.id for message in messages if message.author_id.id == self.id or self.id in [partner.id for partner in message.partner_ids]]
        if not message_ids:
            return True
        return {
            'view_mode': 'tree,form',
            'view_type': 'form',
            'res_model': 'mail.message',
            'type': 'ir.actions.act_window',
            'context': {
                'tree_view_ref': 'sg_mail.view_message_tree',
                'form_view_ref': 'sg_mail.view_message_form'
                },
            'domain': [('id', 'in', message_ids)]
            }
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: