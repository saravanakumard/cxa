# -*- coding: utf-8 -*-
from datetime import datetime

from openerp.osv import fields, osv
from openerp import tools


class sg_mail_report(osv.Model):
    _name = "mi.mail.report"
    _auto = False

    _columns = {
        'create_date': fields.datetime('Creation Date', readonly=True),
        'reply_date': fields.datetime('Reply Date', readonly=True),
        'read_date': fields.datetime('Read Date', readonly=True),
        'read_delay': fields.integer('Read Seconds'),
        'reply_delay': fields.integer('Reply Seconds'),
    }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'mi_mail_report')
        cr.execute("""
                    CREATE OR REPLACE VIEW mi_mail_report AS (
                        SELECT
                            m.id,
                            t.date as create_date,
                            n.read_date as read_date,
                            r.date as reply_date,
                            date_part('epoch', age(t.date, n.read_date)::interval) as read_delay,
                            date_part('epoch', age(r.date, t.date)::interval) as reply_delay
                        FROM
                            public.mail_mail m
                        INNER JOIN
                            public.mail_message t
                        ON
                            m.mail_message_id = t.id
                        LEFT JOIN
                            public.mail_message r
                        ON
                            r.parent_id = t.id
                        LEFT JOIN
                            public.mail_notification n
                        ON
                            n.message_id = t.id
                        WHERE
                            t.type = 'email'
                    )""")
                            # AND t.mail_type = 'in'
