# -*- coding: utf-8 -*-

from openerp.osv import osv, orm, fields
from openerp import tools, api

class mail_message(osv.Model):
    _inherit = 'mail.message'

    def create(self, cr, uid, vals, context=None):
        message_id = super(mail_message, self).create(cr, uid, vals, context=context)
        message = self.browse(cr, uid, message_id)
        author_id = message.author_id.id

        if message.type == 'email' or message.mail_type == 'in':

            fetchmail_server_id = context.get('fetchmail_server_id', None)
            if fetchmail_server_id:
                server = self.pool.get('fetchmail.server').browse(cr, uid, fetchmail_server_id, context=context)
                if server and server.id:
                    if server.partner_id and server.partner_id.id:

                        mail_obj = self.pool.get('mail.mail')
                        mail_ids = mail_obj.search(cr, uid, [('mail_message_id', '=', message.id)])
                        if len(mail_ids) > 0:
                            mail_obj.write(cr, uid, mail_ids, {'recipient_ids': [(4, [server.partner_id.id])]})
                        else:
                            mail_vals = {
                                'name': message.subject or message.record_name,
                                'description': message.body,
                                'recipient_ids': [(4, [server.partner_id.id])],
                                'subject': message.subject or message.record_name,
                                'author_id': author_id,
                                'email_from': message.email_from,
                                'body_html': message.body,
                                'state': 'received',
                            }
                            mail_id = mail_obj.create(cr, uid, mail_vals, {'mail_notrack': True})
                            mail = mail_obj.browse(cr, uid, mail_id)
                            if mail.id and mail.mail_message_id and mail.mail_message_id.id:
                                cr.execute("UPDATE mail_message SET mail_type='in' WHERE id=%s" %(mail.mail_message_id.id,))
                        message.write({'partner_ids': [(4, [server.partner_id.id])]})

        message.write({'author_id': False})
        message.write({'author_id': author_id})
        return message_id

    def _check_mail(self, cr, uid, ids, name, arg, context=None):
        res = {}
        mail_obj = self.pool.get('mail.mail')
        for message in self.browse(cr, uid, ids):
            mail_ids = mail_obj.search(cr, uid, [('mail_message_id', '=', message.id)])
            res[message.id] = mail_ids and True or False
        return res

    def _get_messages(self, cr, uid, ids, context=None):
        result = {}
        for mail in self.pool.get('mail.mail').browse(cr, uid, ids, context=context):
            result[mail.mail_message_id.id] = True
        return result.keys()

    def _mail_type(self, cr, uid, ids, name, arg, context=None):
        res = {}
        mail_obj = self.pool.get('mail.mail')
        for message in self.browse(cr, uid, ids):
            type = 'none'
            mail_ids = mail_obj.search(cr, uid, [('mail_message_id', '=', message.id)])
            if mail_ids:
                mail = mail_obj.browse(cr, uid, mail_ids[0])
                if mail.state == 'exception':
                    type = 'outbox'
                elif mail.state == 'sent':
                    type = 'out'
                elif mail.fetchmail_server_id:
                    type = mail.fetchmail_server_id and 'in' or 'out'
            res[message.id] = type
        return res

    _columns = {
        'has_mail': fields.function(_check_mail, type='boolean', string='Has Mail',
            store = {
                'mail.message': (lambda self, cr, uid, ids, c={}: ids, ['author_id', 'partner_ids', 'notified_partner_ids'], 10),
                'mail.mail': (_get_messages, ['mail_message_id'], 10),
            }),
        'mail_type': fields.function(_mail_type, type='selection', string='Mail Type',
            selection=[('in', 'Incoming'), ('out', 'Outgoing'), ('none', 'None'), ('outbox', 'Outbox')],
            store = {
                'mail.message': (lambda self, cr, uid, ids, c={}: ids, ['author_id', 'partner_ids', 'notified_partner_ids'], 10),
                'mail.mail': (_get_messages, ['fetchmail_server_id', 'state'], 10),
            }),
    }

class mail_mail(osv.Model):
    _inherit = 'mail.mail'

    def onchange_template_id(self, cr, uid, ids, template_id, context=None):
        """ - mass_mailing: we cannot render, so return the template values
            - normal mode: return rendered values """
        if template_id:
            fields = ['subject', 'body_html', 'email_from', 'reply_to', 'mail_server_id']
            template = self.pool['email.template'].browse(cr, uid, template_id, context=context)
            values = dict((field, getattr(template, field)) for field in fields if getattr(template, field))
            if template.attachment_ids:
                values['attachment_ids'] = [att.id for att in template.attachment_ids]
            if template.mail_server_id:
                values['mail_server_id'] = template.mail_server_id.id
            if (template.user_signature or context.get('default_user_signature', False)) and 'body_html' in values:
                signature = self.pool.get('res.users').browse(cr, uid, uid, context).signature
                values['body_html'] = tools.append_content_to_html(values['body_html'], signature, plaintext=False)
        else:
            default_context = dict(context, default_model='mail.mail')
            default_values = self.default_get(cr, uid,
                                              ['composition_mode', 'model', 'res_id', 'parent_id', 'partner_ids',
                                               'subject', 'body', 'email_from', 'reply_to', 'attachment_ids',
                                               'mail_server_id'], context=default_context)
            values = dict((key, default_values[key]) for key in
                          ['subject', 'body', 'partner_ids', 'email_from', 'reply_to', 'attachment_ids',
                           'mail_server_id'] if key in default_values)
        return {'value': values}


    _columns = {
        'template_id': fields.many2one('email.template', 'Use template', select=True),
    }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
