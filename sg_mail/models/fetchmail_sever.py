# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class fetchmail_server(osv.osv):

    _inherit = 'fetchmail.server'

    _columns = {
        'partner_id' : fields.many2one('res.partner',string='Partner'),
    }